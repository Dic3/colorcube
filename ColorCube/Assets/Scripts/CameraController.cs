﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script for moving the main camera according to player's position

public class CameraController : MonoBehaviour {

	public float yMargin;
	public float ySmooth;
	public Vector2 yMax;
	public Vector2 yMin;

	private Transform player;

	void Start(){
	
		player = GameObject.FindGameObjectWithTag ("Player").transform;
	}

	bool CheckYmargin(){

		return Mathf.Abs (transform.position.y - player.position.y) > yMargin;
	}
		
	void FixedUpdate () {

		TrackPlayer ();
	}

	void TrackPlayer(){
	
		float targetY = transform.position.y;

		if (CheckYmargin ()) {
		
			targetY = Mathf.Lerp (transform.position.y, player.position.y, ySmooth = Time.deltaTime);
		}

		targetY = Mathf.Clamp (targetY, yMin.y, yMax.y);

		transform.position = new Vector3 (transform.position.x, targetY, transform.position.z);
	}
}
