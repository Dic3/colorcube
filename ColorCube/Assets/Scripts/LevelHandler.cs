﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHandler : MonoBehaviour {

	public static LevelHandler instance = null;
	private static int levelNumber = 1;
	private static string levelString;

	void Awake()
	{
		//Check if instance already exists
		if (instance == null)

			//if not, set instance to this
			instance = this;

		//If instance already exists and it's not this:
		else if (instance != this)

			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject); 
		
		DontDestroyOnLoad (gameObject);

	}
		

	public static string getLevelString(){
		return levelString;
	}

	public static void IncrealLevelNumber(){
	
		levelNumber++;
		SetLevelString ();
	}

	static void SetLevelString(){
		if (levelNumber == 1) {
			levelString = "a11111b0000000000022226000000000000007333300000000000055559000000000000008444a00000c";
		} else if (levelNumber == 2) {
			levelString = "a11111b0000000000022226000000000000007333300000000000055559000000000000008444a00000c";
		} else if (levelNumber == 3) {
			levelString = "a11111b0000000000022226000000000000007333300000000000055559000000000000008444a00000c";
		} else {
			levelString = "a11111b0000000000022226000000000000007333300000000000055559000000000000008444a00000c";
		}
	}

	


	// Use this for initialization
	void Start () {
		SetLevelString ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
