﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Script for moving the player

public class PlayerMovement : MonoBehaviour {

 	 Vector2 pressStartPos;
	 Vector2 pressEndPos;
	 Vector2 swipe;

	// Jump force
	public float JumpForce;

	public static int colorStatus;

	public Rigidbody2D rb;

	public Vector3 playerVelocity;

	void Start () {


		colorStatus = 0;
		rb = gameObject.GetComponent<Rigidbody2D> ();
		JumpForce = LevelMaker.screenWidth * 30f + 250f;
	}


	void Update () {

		CheckInput ();
		CheckBorderCollision ();
		CheckIfRotate ();
	}

	public void CheckInput(){

		if (Input.GetMouseButtonDown (0)) {
			pressStartPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		}

		if (Input.GetMouseButtonUp (0)) {
			pressEndPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		
			swipe = new Vector2 (pressEndPos.x - pressStartPos.x, pressEndPos.y - pressStartPos.y);

			swipe.Normalize ();

			// Swipe up
			if (swipe.y > 0 && swipe.x > -0.5f && swipe.x < 0.5f) {
				Debug.Log ("UP");
				GetComponent<Rigidbody2D> ().AddForce(new Vector2(0,JumpForce));

			}

			// Swipe left
			if (swipe.x < 0 && swipe.y > -0.5f && swipe.y < 0.5f) {
				Debug.Log ("LEFT");
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (-1 * 4f, GetComponent<Rigidbody2D> ().velocity.y);


			}

			//Swipe right
			if (swipe.x > 0 && swipe.y > -0.5f && swipe.y < 0.5f) {
				Debug.Log ("RIGHT");
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (1 * 4, GetComponent<Rigidbody2D> ().velocity.y);

			}

			// Tap
			if (swipe.x > -0.5f && swipe.x < 0.5f && swipe.y > -0.5f && swipe.y < 0.5f) {
				Debug.Log ("TAP");
				gameObject.GetComponent<Transform> ().Rotate (Vector3.forward * -90);

			}

		}
	
	}

	public void CheckBorderCollision(){

		Transform t = gameObject.GetComponent<Transform>();

		if(t.position.x > (LevelMaker.screenWidth/2)-(LevelMaker.playerSize/2)){
			t.position = new Vector3 (((LevelMaker.screenWidth/2)-(LevelMaker.playerSize/2)),t.position.y,0);
			rb.freezeRotation = true;
		}

		if (t.position.x < -(LevelMaker.screenWidth / 2) + (LevelMaker.playerSize / 2)) {
			t.position = new Vector3 (-((LevelMaker.screenWidth / 2) - (LevelMaker.playerSize / 2)), t.position.y, 0);
			rb.freezeRotation = true;
		}
	}


	void OnCollisionStay2D(Collision2D coll){

		Transform t = gameObject.GetComponent<Transform>();

		if (coll.gameObject.tag == "Redt" && coll.gameObject.GetComponent<TileColourHandler>().isWalkable != true) {
			t.position = LevelMaker.playerStartPosition;
		}
		if (coll.gameObject.tag == "Greent" && coll.gameObject.GetComponent<TileColourHandler>().isWalkable != true) {
			t.position = LevelMaker.playerStartPosition;
		}
		if (coll.gameObject.tag == "Bluet" && coll.gameObject.GetComponent<TileColourHandler>().isWalkable != true) {
			t.position = LevelMaker.playerStartPosition;
		}
		if (coll.gameObject.tag == "Yellowt" && coll.gameObject.GetComponent<TileColourHandler>().isWalkable != true) {
			t.position = LevelMaker.playerStartPosition;
		}
		if (coll.gameObject.tag == "End") {
			t.position = LevelMaker.playerStartPosition;
			LevelHandler.IncrealLevelNumber ();
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	
	}
		

	public void CheckIfRotate(){
		playerVelocity = rb.velocity;

		if (playerVelocity.x != 0) {
			rb.freezeRotation = false;
		} else {
			rb.freezeRotation = true;
		}
	}

}


