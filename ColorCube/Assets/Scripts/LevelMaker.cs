﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Script for generating levels

public class LevelMaker : MonoBehaviour {

	// Columns and rows for the tile grid
	public int columns;
	public int rows;

	// X and y values of tile position
	public float xPos;
	public float yPos;

	// Position for tile
	public Vector3 tilePosition;

	// Position of first tile
	public Vector3 startPosition;

	// Tile prefabs
	public GameObject empty_tile;
	public GameObject grey_tile;
	public GameObject red_tile;
	public GameObject blue_tile;
	public GameObject green_tile;
	public GameObject yellow_tile;
	public GameObject red_potion;
	public GameObject blue_potion;
	public GameObject green_potion;
	public GameObject yellow_potion;
	public GameObject start_end_tile;
	public GameObject start_sign;
	public GameObject finish_sign;

	// Width of the screen
	public static float screenWidth;

	// Tile size
	public float tileSize;

	// Tile to be instantiated
	public GameObject tileToInstantiate;

	// Counts used columns
	public int columnCounter;

	// Main camera
	public Camera camera;

	// String to generate the level
	public string world;

	// ID for selecting righ tile
	public char tilePrefabID;

	// For reading the level string
	public int stringCounter;

	// Player's start position
	public static Vector3 playerStartPosition;

	// Palyer object
	public GameObject player;

	// Size of a player
	public static float playerSize;

	// Arrays for holding tiles
	private static GameObject[] red_tiles;
	private static GameObject[] green_tiles;
	private static GameObject[] blue_tiles;
	private static GameObject[] yellow_tiles;

	void Start () {

		world = "a11111b0000000000022226000000000000007333300000000000055559000000000000008444a00000c";//LevelHandler.getLevelString ();
		//player = GameObject.FindGameObjectWithTag ("Player");
		stringCounter = 0;
		columns = 6;
		camera = gameObject.GetComponent<Camera> ();
		startPosition = camera.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f));
		tileToInstantiate = empty_tile;
		screenWidth = (Camera.main.orthographicSize * 2) * Screen.width / Screen.height;
		tileSize = screenWidth / columns;
		xPos = startPosition.x + (tileSize/2);
		yPos = startPosition.y + (tileSize/2);
		tilePosition = new Vector3 (xPos, yPos, 0f);
		CalculatePlayerSize ();

		for (int i = 0; i < 84; i++) {
			ReadWorldString ();
			tileToInstantiate.GetComponent<Transform> ().localScale = new Vector3 (screenWidth / columns, screenWidth / columns, 0);
			GameObject tile = Instantiate (tileToInstantiate, tilePosition, Quaternion.identity) as GameObject;
			CalculteNewTilePosition ();
		}

		playerStartPosition = GameObject.FindGameObjectWithTag ("Start").GetComponent<Transform> ().position;
		Instantiate (player, playerStartPosition, Quaternion.identity);
		player.GetComponent<Transform> ().localScale = new Vector3 (playerSize, playerSize, 0);
		gameObject.GetComponent<CameraController> ().enabled = true;
		red_tiles = GameObject.FindGameObjectsWithTag ("Redt");
		green_tiles = GameObject.FindGameObjectsWithTag ("Greent");
		blue_tiles = GameObject.FindGameObjectsWithTag ("Bluet");
		yellow_tiles = GameObject.FindGameObjectsWithTag ("Yellowt");

	}

	// calcualtes position for next tile to instantiate
	void CalculteNewTilePosition(){

		columnCounter++;
		xPos = xPos + tileSize;

		if (columnCounter == columns) {
			yPos = yPos + tileSize;
			xPos = startPosition.x + (tileSize/2);
			columnCounter = 0;
		}

		tilePosition = new Vector3 (xPos, yPos, 0f);
	}

	// reads the string for tiles
	void ReadWorldString(){

		int a = stringCounter;
		tilePrefabID = world [a];

		if (tilePrefabID == '0') {
			tileToInstantiate = empty_tile;
		} else if (tilePrefabID == '1') {
			tileToInstantiate = grey_tile;
		} else if (tilePrefabID == '2') {
			tileToInstantiate = red_tile;
		} else if (tilePrefabID == '3') {
			tileToInstantiate = blue_tile;
		} else if (tilePrefabID == '4') {
			tileToInstantiate = green_tile;
		} else if (tilePrefabID == '5') {
			tileToInstantiate = yellow_tile;
		} else if (tilePrefabID == '6') {
			tileToInstantiate = red_potion;
		} else if (tilePrefabID == '7') {
			tileToInstantiate =  blue_potion;
		} else if (tilePrefabID == '8') {
			tileToInstantiate =  green_potion;
		} else if (tilePrefabID == '9') {
			tileToInstantiate =  yellow_potion;
		} else if (tilePrefabID == 'a') {
			tileToInstantiate =  start_end_tile;
		} else if (tilePrefabID == 'b') {
			tileToInstantiate =  start_sign;
		} else if (tilePrefabID == 'c') {
			tileToInstantiate =  finish_sign;
		} else {
			tileToInstantiate = empty_tile;
		}

		stringCounter++;
	}

	// Function for calculating the size for the player object
	public void CalculatePlayerSize(){
		
		camera = gameObject.GetComponent<Camera> ();
		screenWidth = (Camera.main.orthographicSize * 2) * Screen.width / Screen.height;
		playerSize = screenWidth / columns;
		//player.GetComponent<Transform> ().localScale = new Vector3 (playerSize, playerSize, 0);
	}

	void Update(){

		if (Input.GetKey (KeyCode.Space)) {
			LevelHandler.IncrealLevelNumber ();
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			gameObject.GetComponent<CameraController> ().enabled = true;
		}
	}

	public static GameObject[] GetTiles(string colour){

		string c = colour;
		GameObject[] tiles;

		if (c == "red") {
			tiles = red_tiles;
		} else if (c == "green") {
			tiles = green_tiles;
		} else if (c == "blue") {
			tiles = blue_tiles;
		} else if (c == "yellow") {
			tiles = yellow_tiles;
		} else {
			tiles = red_tiles;
		}
		return tiles;
	}
}