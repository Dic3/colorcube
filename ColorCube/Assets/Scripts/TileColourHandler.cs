﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script for handling colours and animations of tiles

public class TileColourHandler : MonoBehaviour {

	// Tile's animator component
	private Animator animator;

	// Indicates if tile is walkable or not
	public bool isWalkable;

	void Start () {

		animator = gameObject.GetComponent<Animator> ();
		isWalkable = false;
	}

	IEnumerator FadeToGray(){
		animator.SetBool ("FadeToGray", true);
		yield return new WaitForSeconds (animator.GetCurrentAnimatorClipInfo (0).Length);
		animator.SetBool ("FadeToGray", false);
		yield return new WaitForSecondsRealtime (5);
		animator.SetBool ("Flash", true);
		yield return new WaitForSeconds (animator.GetCurrentAnimatorClipInfo (0).Length);
		animator.SetBool ("Flash", false);
		isWalkable = false;
	}

	public void FadeGray(){
		isWalkable = true;
		StartCoroutine (FadeToGray());
	}
}
