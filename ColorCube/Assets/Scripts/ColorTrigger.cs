﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorTrigger : MonoBehaviour {

	private float returnTime;
	private TileColourHandler tile_ch;
	public GameObject[] red_tiles;
	public GameObject[] green_tiles;
	public GameObject[] blue_tiles;
	public GameObject[] yellow_tiles;
	public string currentStatus;
	public string previousStatus;
	//private string red, green, yellow, blue;

	void Start () {

		returnTime = 10f;
		red_tiles = LevelMaker.GetTiles ("red");
		green_tiles = LevelMaker.GetTiles ("green");
		blue_tiles = LevelMaker.GetTiles ("blue");
		yellow_tiles = LevelMaker.GetTiles ("yellow");
		//red = "red";
		//green = "green";
		//yellow = "yellow";
		//blue = "blue";
	}

	void OnTriggerEnter2D (Collider2D other){

		if (gameObject.tag == "Redp") {
			foreach (GameObject r in red_tiles) {
				tile_ch = r.GetComponent<TileColourHandler> ();
				tile_ch.FadeGray ();
				//PotionStatus.SetCurrentPotion (red);
				//gameObject.SetActive (false);

			}

		} else if (gameObject.tag == "Greenp") {
			foreach (GameObject r in green_tiles) {
				tile_ch = r.GetComponent<TileColourHandler> ();
				tile_ch.FadeGray ();
				//PotionStatus.SetCurrentPotion (green);
				//gameObject.SetActive (false);
			}
		

		} else if (gameObject.tag == "Bluep") {
			foreach (GameObject r in blue_tiles) {
				tile_ch = r.GetComponent<TileColourHandler> ();
				tile_ch.FadeGray ();
				//PotionStatus.SetCurrentPotion (blue);
				//gameObject.SetActive (false);
			}
		

			//PlayerMovement.colorStatus = 3;
		} else if (gameObject.tag == "Yellowp") {
			foreach (GameObject r in yellow_tiles) {
				tile_ch = r.GetComponent<TileColourHandler> ();
				tile_ch.FadeGray ();
				//PotionStatus.SetCurrentPotion (yellow);
				//gameObject.SetActive (false);
			}



		} else {
			PlayerMovement.colorStatus = 0;
		}
		gameObject.SetActive (false);
	}

	void Update(){
		currentStatus = PotionStatus.GetCurrentPotion ();
		previousStatus = PotionStatus.GetPreviousPotion ();
	}

	private void FadePrevious(){


		
	}

	/*IEnumerator ReturnPlayerStatus(){
		
			yield return new WaitForSeconds (returnTime);
			foreach (GameObject r in blue_tiles) {
			tile_ch = r.GetComponent<TileColourHandler> ();
			tile_ch.FadeBackToOriginal ();
		}
		Debug.Log ("NotWalkable");
	}*/
}
